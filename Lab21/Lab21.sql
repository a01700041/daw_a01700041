
  /*Primer problema*/

   IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'creaMaterial' AND type = 'P')
                DROP PROCEDURE creaMaterial
            GO

            CREATE PROCEDURE creaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
            GO

   EXECUTE creaMaterial 5000,'Martillos Acme',250,15


   SELECT *
   FROM Materiales;

    /*Modificar Material*/
 Select *
 From Materiales

  IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modMaterial' AND type = 'P')
                DROP PROCEDURE modMaterial
            GO

            CREATE PROCEDURE modMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                UPDATE Materiales
                SET Descripcion = @udescripcion, Costo = @ucosto, PorcentajeImpuesto = @uimpuesto
                WHERE Clave= @uclave
            GO

    EXECUTE modMaterial 5000, 'Martilocos', 500, 2.18

Select *
From Materiales

  IF EXISTS (SELECT name FROM sysobjects
                      WHERE name = 'eliMaterial' AND type = 'P')
              DROP PROCEDURE eliMaterial
           GO

           CREATE PROCEDURE eliMaterial
               @uclave NUMERIC(5,0)
           AS
               DELETE FROM Materiales WHERE Clave = @uclave
           GO

EXECUTE eliMaterial 5000

Select *
From Materiales


/*Parte 2*/

 IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'agrPermiso' AND type = 'P')
                DROP PROCEDURE agrPermiso
            GO

            CREATE PROCEDURE agrPermiso
                @udescripcion VARCHAR(MAX)
            AS
                INSERT INTO Permiso VALUES (@udescripcion);


  IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'crProyecto' AND type = 'P')
                DROP PROCEDURE crProyecto
            GO

            CREATE PROCEDURE crProyecto
                @unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)
            AS
                INSERT INTO Proyectos VALUES(@unumero, @udenominacion)
            GO

 EXECUTE crProyecto 7677,'Martitios'

 IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modProyecto' AND type = 'P')
                DROP PROCEDURE modProyecto
            GO

            CREATE PROCEDURE modProyecto
                @unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)

            AS
                UPDATE Proyectos
                SET Denominacion = @udenominacion
                WHERE Numero= @unumero

 EXECUTE modProyecto 7677,'Martotes'

  IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'elimProyecto' AND type = 'P')
                DROP PROCEDURE elimProyecto
            GO

            CREATE PROCEDURE elimProyecto
                @unumero NUMERIC(5,0)
            AS
                DELETE FROM Proyectos WHERE Numero = @unumero
            GO

EXECUTE elimProyecto 7677



 IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'creProveedor' AND type = 'P')
                DROP PROCEDURE creProveedor
            GO

            CREATE PROCEDURE creProveedor
                @urfc CHAR(13),
                @urazonsocial VARCHAR(50)
            AS
                INSERT INTO Proveedores VALUES(@urfc, @urazonsocial)
            GO

 EXECUTE creProveedor 'GHYJI786GDEWS','NOtaa'

 IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modProveedor' AND type = 'P')
                DROP PROCEDURE modProveedor
            GO

            CREATE PROCEDURE modProveedor
                @urfc CHAR(13),
                @urazonsocial VARCHAR(50)

            AS
                UPDATE Proveedores
                SET RazonSocial = @urazonsocial
                WHERE RFC= @urfc

 EXECUTE modProveedor 'GHYJI786GDEWS','mfyt'

  IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'elimProveedor' AND type = 'P')
                DROP PROCEDURE elimProveedor
            GO

            CREATE PROCEDURE elimProveedor
               @urfc CHAR(13)
            AS
                DELETE FROM Proveedores WHERE RFC = @urfc
            GO

EXECUTE elimProveedor 'GHYJI786GDEWS'


 IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'creEntrega' AND type = 'P')
                DROP PROCEDURE creEntrega
            GO

            CREATE PROCEDURE creEntrega
                @uclave NUMERIC (5,0),
                @urfc CHAR(13),
                @unumero NUMERIC (5,0),
                @ufecha DATETIME,
                @ucantidad NUMERIC(8,2)
            AS
                INSERT INTO Entregan VALUES(@uclave,@urfc,@unumero,@ufecha,@ucantidad)
            GO

 EXECUTE creEntrega 7777,'GGGGGGGGGGGGG',8888,'2017-10-23 00:00:00.000',45.00

 IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modEntrega' AND type = 'P')
                DROP PROCEDURE modEntrega
            GO

            CREATE PROCEDURE modEntrega
                @uclave NUMERIC (5,0),
                @urfc CHAR(13),
                @unumero NUMERIC (5,0),
                @ufecha DATETIME,
                @ucantidad NUMERIC(8,2)

            AS
                UPDATE Entregan
                SET Fecha = @ufecha, Cantidad = @ucantidad
                WHERE Clave = @uclave AND RFC = @urfc AND Numero = @unumero

 EXECUTE modEntrega 7777,'GGGGGGGGGHHHG',8888,'2017-10-23 01:00:00.000',145.00

  IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'eliEntrega' AND type = 'P')
                DROP PROCEDURE eliEntrega
            GO

            CREATE PROCEDURE eliEntrega
                @uclave NUMERIC (5,0),
                @urfc CHAR(13),
                @unumero NUMERIC (5,0)
            AS
                DELETE FROM Entregan WHERE Clave = @uclave AND RFC = @urfc AND Numero = @unumero
            GO

EXECUTE eliEntrega 7777,'GGGGGGGGGHHHG',8888

   IF EXISTS (SELECT name FROM sysobjects
                                       WHERE name = 'queryMaterial' AND type = 'P')
                                DROP PROCEDURE queryMaterial
                            GO

                            CREATE PROCEDURE queryMaterial
                                @udescripcion VARCHAR(50),
                                @ucosto NUMERIC(8,2)

                            AS
                                SELECT * FROM Materiales WHERE descripcion
                                LIKE '%'+@udescripcion+'%' AND costo > @ucosto
                            GO

  EXECUTE queryMaterial 'Lad',20