
SELECT *FROM Materiales;
SELECT *FROM Entregan;
SELECT *FROM Proyectos;
SELECT *FROM Proveedores;



/*1.- La suma de las cantidades e importe total de todas las entregas realizadas durante el 97. */
SELECT COUNT(E.Clave) AS 'Cantidades',SUM(E.Cantidad*(M.Costo + M.PorcentajeImpuesto)) AS 'Importe Total'
FROM Materiales M, Entregan E
WHERE M.Clave = E.Clave
AND E.Fecha BETWEEN '01/01/1997'
AND '31/12/1997';

/*2.- Para cada proveedor, obtener la razón social del proveedor, número de entregas e importe total de las entregas realizadas. */
SELECT P.RazonSocial, COUNT(RFC) AS 'Numero de entregas', SUM(E.Cantidad*(M.Costo +M.PorcentajeImpuesto)) as 'Importe Total'
FROM Proveedores P, Materiales M, Entregan E
WHERE P.RFC = E.RFC
AND M.Clave = E.Clave
GROUP BY E.RFC, P.RazonSocial;

/*3.- Por cada material obtener la clave y descripción del material, la cantidad total entregada, la mínima cantidad entregada, la máxima cantidad entregada, el importe total de las entregas de aquellos materiales en los que la cantidad promedio entregada sea mayor a 400.*/
SELECT M.Clave, M.Descripcion, SUM(E.Cantidad) AS 'Cantidad total entregada', MIN(E.Cantidad) AS 'Mínima cantidad entregada', MAX(E.Cantidad), SUM(E.Cantidad*(M.Costo + M.PorcentajeImpuesto)) AS 'Importe Total [Promedio + 400]'
FROM Materiales M, Entregan E
WHERE M.Clave = E.Clave
GROUP BY M.Clave, M.Descripcion
HAVING AVG(E.Cantidad) > 400;

/*4.- Para cada proveedor, indicar su razón social y mostrar la cantidad promedio de cada material entregado, detallando la clave y descripción del material, excluyendo aquellos proveedores para los que la cantidad promedio sea menor a 500.*/
CREATE VIEW M_RazonSocial_C AS (
SELECT P.RazonSocial, AVG(E.Cantidad)AS Promedio_E, E.Clave
FROM Proveedores P,  Entregan E
WHERE E.RFC = P.RFC
GROUP BY E.RFC,P.RazonSocial,E.Clave)

SELECT R.RazonSocial,r.Promedio_E,M.Clave,M.Descripcion
FROM M_RazonSocial_C R, Materiales M
WHERE m.Clave = R.Clave
AND R.Promedio_E >= 500


/*5.- Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos grupos de proveedores: aquellos para los que la cantidad promedio entregada es menor a 370 y aquellos para los que la cantidad promedio entregada sea mayor a 450.*/
CREATE VIEW M_RazonSocial_C AS (
SELECT P.RazonSocial, AVG(E.Cantidad)AS Promedio_E, E.Clave
FROM Proveedores P,  Entregan E
WHERE E.RFC = P.RFC
GROUP BY E.RFC,P.RazonSocial,E.Clave)

SELECT R.RazonSocial,R.Promedio_E,M.Clave,M.Descripcion
FROM M_RazonSocial_C R, Materiales M
WHERE m.Clave = R.Clave
AND (R.Promedio_E < 370 or R.Promedio_E > 450)
ORDER BY Promedio_E;

/*Insertar 5 Nuevos Materiales*/
INSERT INTO Materiales VALUES (2000,'Madera', 500, 2.15);
INSERT INTO Materiales VALUES (2010,'Marmol', 500, 2.15);
INSERT INTO Materiales VALUES (2020,'Hierro', 500, 2.15);
INSERT INTO Materiales VALUES (2030,'Tierra', 500, 2.15);
INSERT INTO Materiales VALUES (2040,'Roca', 500, 2.15);

/*6.- Clave y descripción de los materiales que nunca han sido entregados.*/
CREATE VIEW Materiales_NE(
SELECT M.Clave
FROM Materiales M
EXCEPT
SELECT E.Clave
FROM Entregan E, Materiales M
WHERE E.Clave = M.Clave
GROUP BY E.Clave
)

SELECT N.Clave, M.Descripcion
FROM Materiales_NE N, Materiales M
WHERE N.Clave = M.Clave;

/*7.- Razón social de los proveedores que han realizado entregas tanto al proyecto 'Vamos México' como al proyecto 'Querétaro Limpio'.*/
CREATE VIEW Entregas_MQ AS(
SELECT E.RFC
FROM Proyectos P, Entregan E
WHERE E.Numero = P.Numero
AND (P.Denominacion = 'Vamos Mexico' or P.Denominacion = 'Queretaro Limpio')
GROUP BY E.RFC
)

Select P.RazonSocial
FROM Entregas_MQ Q , Proveedores P
WHERE Q.RFC = P.RFC

/*8.- Descripción de los materiales que nunca han sido entregados al proyecto 'CIT Yucatán'.*/
CREATE VIEW Materiales_NY AS (
SELECT M.Clave
FROM Materiales M
    EXCEPT
SELECT E.Clave
FROM Entregan E, Proyectos P
WHERE  E.Numero = P.Numero
AND P.Denominacion = 'CIT Yucatan'
GROUP BY E.Clave)

SELECT M.Descripcion
FROM Materiales M, Materiales_NY Y
WHERE Y.Clave =M.Clave;

/*9.- Razón social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad entregada es mayor al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'. */
CREATE VIEW Promedio_EP AS (
SELECT E.RFC, AVG(E.Cantidad) as Promedio_C
FROM Entregan E
GROUP BY E.RFC)

SELECT P.RazonSocial , EP.Promedio_C
FROM Proveedores P, Promedio_EP EP
WHERE P.RFC = EP.RFC AND EP.Promedio_C >(
  SELECT EP.Promedio_C
  FROM Promedio_EP EP
  WHERE RFC ='VAGO780901'
);

/*10.- RFC, razón social de los proveedores que participaron en el proyecto 'Infonavit Durango' y cuyas cantidades totales entregadas en el 2000 fueron mayores a las cantidades totales entregadas en el 2001.*/


CREATE VIEW Proveedores_D AS (
SELECT E.RFC, E.Cantidad, E.Fecha
FROM Entregan E, Proyectos P
WHERE P.Numero = E.Numero
AND P.Denominacion = 'Infonavit Durango'
)

CREATE VIEW PD_2000 AS (
SELECT RFC, Cantidad
FROM Proveedores_D
WHERE Fecha BETWEEN '01/01/2000' AND '31/12/2000'
)

CREATE VIEW PD_2001 AS (
SELECT RFC, Cantidad
FROM Proveedores_D
WHERE Fecha BETWEEN '01/01/2001' AND '31/12/2001'
)

CREATE  VIEW Proveedores_D_Mayores AS (
SELECT P0.RFC
FROM PD_2000 P0, PD_2001 P1
WHERE P0.RFC = P1.RFC
AND P0.Cantidad > P1.Cantidad
)

SELECT PDM.RFC,P.RazonSocial
FROM Proveedores_D_Mayores PDM, Proveedores P
WHERE P.RFC = PDM.RFC;