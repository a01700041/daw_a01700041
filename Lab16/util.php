<?php

    function conectDb(){
      $servername = "localhost";
      $username = "root";
      $password = "";
      $db = "SportsStore";

      $con= mysqli_connect($servername, $username, $password, $db);

      //Esto va a checar la conexión
      if (!$con) {
        die("connection failed: " . mysqli_connect_error());
      }

      return $con;

    }

    //la variable $mysql es una conexión establecida anteriormente

    function closeDB($mysql){
      mysqli_close($mysql);
    }

    function getInput($db, $registroId){
    //Specification of the SQL query
    $query='SELECT * FROM Kit WHERE id="'.$registroId.'"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    return $fila;
}

    function getKits(){
      $conn = conectDb();
      $sql = "SELECT id, color, price, club FROM Kit";
      $result = mysqli_query($conn, $sql);

      if(mysqli_num_rows($result) > 0){
          echo '<table class="ui table" ><tr><th>ID</th><th>Color</th><th>Price</th><th>Club</th><th></th><th></th></tr>';
          while($row = mysqli_fetch_assoc($result)){
              echo "<tr>";
              echo "<td>".$row["id"]."</td>";
              echo "<td>".$row["color"]."</td>";
              echo "<td>$".$row["price"]."</td>";
              echo "<td>".$row["club"]."</td>";
              echo '<td><a href="edit.php?id='.$row["id"].'">Edit</a></td>';
              echo '<td><a href="delete.php?id='.$row["id"].'">Delete</a></td>';
              echo "</tr>";
            }

          echo "</table>";
      }
      mysqli_free_result($result);

      closeDb($conn);

    }


    function getKitByColor($ColorName){
      $conn = conectDb();
      $sql = "SELECT id, color, price, club FROM Kit WHERE color LIKE '%".$ColorName."%'";
      $result = mysqli_query($conn, $sql);

      if(mysqli_num_rows($result) > 0){
          echo '<table class="ui table" ><tr><th>Color</th><th>Price</th><th>Club</th></tr>';
          while($row = mysqli_fetch_assoc($result)){
              echo "<tr>";
              echo "<td>".$row["color"]."</td>";
              echo "<td>$".$row["price"]."</td>";
              echo "<td>".$row["club"]."</td>";
              echo "</tr>";
          }
          echo "</table>";
      }
      mysqli_free_result($result);

      closeDb($conn);

    }

    function getCheapestKits($cheap_price){
      $conn = conectDb();
      $sql = "SELECT id, color, price, club FROM Kit WHERE price <= '".$cheap_price."'";
      $result = mysqli_query($conn, $sql);

        if(mysqli_num_rows($result) > 0){
          echo '<table class="ui table" ><tr><th>Color</th><th>Price</th><th>Club</th></tr>';
          while($row = mysqli_fetch_assoc($result)){
              echo "<tr>";
              echo "<td>".$row["color"]."</td>";
              echo "<td>$".$row["price"]."</td>";
              echo "<td>".$row["club"]."</td>";
              echo "</tr>";
          }
          echo "</table>";

      }
      mysqli_free_result($result);
      closeDb($conn);
    }


    function saveInput($color,$price,$club){


      $db = conectDb();

      // insert command specification
    $query='INSERT INTO kit (`color`, `price` ,`club`) VALUES (?,?,?) ';
    // Preparing the statement
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params

    if (!$statement->bind_param("sss", $color, $price, $club)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     }

        closeDb($db);

    }
    function editInput($id, $color,$price,$club){
        $db = conectDb();
        // insert command specification
        $query='UPDATE kit SET color=?, price=?, club=? WHERE id=?';
        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ssss", $color, $price, $club, $id)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // update execution
        if ($statement->execute()) {
            echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
        closeDb($db);
    }

    Function deleteInput($id){
      $db = conectDb();
      // insert command specification
      $query='DELETE FROM kit WHERE id=?';
      // Preparing the statement
      if (!($statement = $db->prepare($query))) {
          die("Preparation failed: (" . $db->errno . ") " . $db->error);
      }
      // Binding statement params
      if (!$statement->bind_param("s", $id)) {
          die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
      }
      // update execution
      if ($statement->execute()) {
          echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
      } else {
          die("Update failed: (" . $statement->errno . ") " . $statement->error);
      }
      closeDb($db);
    }

 ?>
