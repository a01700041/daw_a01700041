IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')



DROP TABLE Materiales

CREATE TABLE Materiales
(
  Clave numeric(5) not null,
  Descripcion varchar(50),
  Costo numeric (8,2)
)



IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')

DROP TABLE Proveedores

CREATE TABLE Proveedores
(
  RFC char(13) not null,
  RazonSocial varchar(50)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')

DROP TABLE Proyectos


CREATE TABLE Proyectos
(
  Numero numeric(5) not null,
  Denominacion varchar(50)
)
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')

DROP TABLE Entregan

CREATE TABLE Entregan
(
  Clave numeric(5) not null,
  RFC char(13) not null,
  Numero numeric(5) not null,
  Fecha DateTime not null,
  Cantidad numeric (8,2)
)

--EJERCICIO 2


INSERT INTO Materiales values(1000, 'xxx', 1000)

SELECT *
FROM Materiales; --No esta ordenado y hay redudancia de datos porque se repite la llave

Delete from Materiales where Clave = 1000 and Costo = 1000

SELECT *
FROM Materiales; --Ya se elimino el registro

Delete from Materiales where Clave = 1000 and Costo = 1000



ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave)

INSERT INTO Materiales values(1000, 'xxx', 1000) --No me deja porque ya se declaro porque se repite

--EJERCICIO 3

SELECT *
FROM Materiales;

SELECT *
FROM Entregan;

SELECT *
FROM Proveedores;

SELECT *
FROM Proyectos;


INSERT INTO entregan values (0, 'xxx', 0, '1-jan-02', 0) ;
Delete from Entregan where Clave = 0

ALTER TABLE entregan add constraint cfentreganclave
  foreign key (clave) references materiales(clave);

INSERT INTO entregan values (0, 'xxx', 0, '1-jan-02', 0) ; --No me deja, osea bien :)

sp_helpconstraint Materiales
sp_helpconstraint Proveedores
sp_helpconstraint Entregan
sp_helpconstraint Proyectos


ALTER TABLE Proveedores add constraint llaveProvedores PRIMARY KEY (RFC) --Primero tengo que definir la llave primaria

ALTER TABLE Entregan add constraint cfentreganRFC --luego ya puedo conectarlas
  foreign key (RFC) references Proveedores(RFC);


ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero) --Primero tengo que definir la llave primaria

ALTER TABLE Entregan add constraint cfentreganNumero --luego ya puedo conectarlas
  foreign key (Numero) references Proyectos(Numero) ;

--Ejercicio 4

 INSERT INTO entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0);

SELECT *
FROM Entregan; --Puso el registro con la hora actual && no tiene sentido lo del 0 [NO ES GRATIS]

Delete from Entregan where Cantidad = 0

SELECT *
FROM Entregan;

ALTER TABLE entregan add constraint cantidad check (cantidad > 0) ;

INSERT INTO entregan values (1001, 'AAAA800101', 6000, GETDATE(), 0); --No funciona porque no cumple con las restricciones


sp_helpconstraint Materiales
sp_helpconstraint Proveedores
sp_helpconstraint Entregan
sp_helpconstraint Proyectos