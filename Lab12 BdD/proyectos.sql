BULK INSERT a1700041.a1700041.[Proyectos]
  FROM 'e:\wwwroot\a1700041\Proyectos.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

SELECT *
FROM Proyectos;