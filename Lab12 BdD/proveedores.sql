BULK INSERT a1700041.a1700041.[Proveedores]
  FROM 'e:\wwwroot\a1700041\proveedores.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

SELECT *
FROM Proveedores;