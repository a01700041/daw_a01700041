SET DATEFORMAT dmy

BULK INSERT a1700041.a1700041.[Entregan]
  FROM 'e:\wwwroot\a1700041\entregan.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

SELECT *
FROM Entregan;